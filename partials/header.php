<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title>PHP JSON ECOMMERCE</title>
	<script src="https://kit.fontawesome.com/02db094c22.js"></script>
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	</head>

	<body>
		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<div class="container">
				<a class="navbar-brand" href="index.php">
					<i class="fas fa-store-alt"></i> 
					Shop.io
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarResponsive">
					<ul class="navbar-nav ml-auto">
						<!-- Insert Links Here -->
						<li class="nav-item">
							<a class="nav-link" href="login.php">
								<i class="fas fa-lock"></i>
								Login
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="register.php">
								<i class="far fa-user"></i>
								Register
							</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="cart.php">
							<i class="fas fa-shopping-cart"></i> 
							Cart
							<?php
								if(isset($_SESSION["cart"])){
									echo "<span class="badge badge-secondary">
											count($_SESSION['cart'])"
										</span>";
								<!-- echo "count($_SESSION['cart'])"; -->
								}
							?>
						</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="assets/lib/processLogout.php">
								<i class="fas fa-shopping-cart"></i>
								Logout
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>