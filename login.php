<?php 
	session_start();
	require "partials/header.php";
?>


<div class="container">
	<!-- ALERT MESSAGE -->
	<div class="row">
		<div class="col">
			<?php
				if(isset($_SESSION["message"])){
					echo "<div class='alert alert-success alert-dismissible fade show' role='alert' id='success-message'>
							{$_SESSION["message"]}
						</div>";
				unset($_SESSION["message"]);
				//header("Refresh:2");
				}
			?>
		</div>
	</div>

	<!-- LOGIN FORM -->
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">
					<form>
					  <div class="form-group">
						    <label for="exampleInputEmail1">Email address</label>
						    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
						    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
					  </div>
					  <div class="form-group">
						    <label for="exampleInputPassword1">Password</label>
						    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					  </div>
					  <button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




<?php 
	session_start();
	require "partials/footer.php";
?>

