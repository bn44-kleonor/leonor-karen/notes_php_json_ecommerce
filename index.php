<!-- ACT 1
1) Display number products added to cart in Header
Note: Not the total quantity but the number of products only

ACT 2
2) When the card image and card-text are clicked, display product.php page
3) Add product and display success message in product.php page
Note: Recall PHP Referer and strpos
 -->

<?php
	session_start();
	require "partials/header.php";
	//GET OBJECTS FROM PRODUCTS>JSON
	$products_objects = file_get_contents("assets/lib/products.json");
	// var_dump($products_objects);
	//CONVERT TO PHP ARRAY
	$products = json_decode($products_objects, true);
	var_dump($_SESSION);
?>

<style type="text/css">
	img{
		height: 280px;
	}
</style>


<div class="container">
	<div class="row">
		<div class="col">
			<?php
				if(isset($_SESSION["message"])){
					echo "<div class='alert alert-success alert-dismissible fade show' role='alert' id='success-message'>
							{$_SESSION["message"]}
						</div>";
				unset($_SESSION["message"]);
				//header("Refresh:2");
				}
			?>
		</div>
	</div>
	<div class="row">
		<?php
		for ($i=0; $i<count($products); $i++){
		?>
		<div class="col col-md-4 col-lg-3 card-group mb-4">
			<div class="card">
				<img class="card-img-top"
					src="assets/lib/<?php echo $products[$i]["image"];?>"
					alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title">
						<?php echo $products[$i]["name"];?>
					</h5>
					<p class="card-text text-secondary">
						&#8369;
						<?php
							echo number_format($products[$i]["price"], 2, ".", "");
						?>
					</p>
					<p class="card-text text-secondary">
						<?php 
							echo $products[$i]["description"];
						?>
					</p>
					<div class="card-footer">
						<form method="POST" action="assets/lib/processAddToCart.php?productid=<?php echo $i; ?>">
								<input type="number" name="quantity" class="form-control" required>
								<button class="btn btn-primary btn-block">
									Add To Cart
								</button>
						</form>
					</div>
				</div>
			</div>
			<?php 
			}
			?>
		</div>
	</div>
</div>

<?php
require "partials/footer.php";
?>

