<?php

	//1) START A SESSION
	session_start();

	$quantity =  $_POST["quantity"];
	$product_id = $_GET["productid"];
	
	// echo "Quantity: $quantity";
	// echo "<br>";
	// $student_id = $_GET["customerId"];
	// echo "Customer id: $customer_id";
	// echo "<br>";


	//SETTING A SESSION
	// SYNTAX: $_SESSION["cart"] = "value";
	if(isset($_SESSION["cart"][$product_id])){
		$_SESSION["cart"][$product_id] += $quantity;	//notice the '+='
	} else {
		$_SESSION["cart"][$product_id] = $quantity;		//notice the '='
	}


	//var_dump($_SESSION);
	//what: notify user that payment is received. start another session, call it 'message'.
	$_SESSION["message"] = "Product has been added to cart!";
	//var_dump($_SERVER["HTTP_REFERER"]);
	//assign value to $referer;
	//strpos to check if $referer contains "product.php";
	header("Location: ../../index.php");
	exit();
?>
